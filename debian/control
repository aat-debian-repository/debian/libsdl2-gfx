Source: libsdl2-gfx
Maintainer: Debian SDL packages maintainers <pkg-sdl-maintainers@lists.alioth.debian.org>
Uploaders: Felix Geyer <fgeyer@debian.org>,
           Manuel A. Fernandez Montecelo <mafm@debian.org>,
           Gianfranco Costamagna <locutusofborg@debian.org>
Section: libs
Priority: optional
Standards-Version: 4.6.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               libsdl2-dev,
               libtool,
               tar (>= 1.28),
               pkg-config
Build-Depends-Indep: doxygen
Vcs-Git: https://salsa.debian.org/sdl-team/libsdl2-gfx.git
Vcs-Browser: https://salsa.debian.org/sdl-team/libsdl2-gfx
Homepage: https://www.ferzkopp.net/joomla/content/view/19/14/

Package: libsdl2-gfx-1.0-0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: drawing and graphical effects extension for SDL2
 The SDL2_gfx library is an extension to the SDL2 library which provides
 basic antialiased drawing routines such as lines, circles or polygons,
 an interpolating rotozoomer for SDL2 surfaces, framerate control and MMX
 image filters.
 .
 This package contains the SDL2_gfx runtime library.

Package: libsdl2-gfx-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libsdl2-dev,
 libsdl2-gfx-1.0-0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Suggests: libsdl2-gfx-doc
Description: development files for SDL2_gfx
 The SDL2_gfx library is an extension to the SDL2 library which provides
 basic antialiased drawing routines such as lines, circles or polygons,
 an interpolating rotozoomer for SDL2 surfaces, framerate control and MMX
 image filters.
 .
 This package contains the header files and static library needed to
 compile applications that use SDL2_gfx.

Package: libsdl2-gfx-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: libsdl2-gfx-dev (= ${binary:Version})
Description: documentation files for SDL2_gfx
 The SDL2_gfx library is an extension to the SDL2 library which provides
 basic antialiased drawing routines such as lines, circles or polygons,
 an interpolating rotozoomer for SDL2 surfaces, framerate control and MMX
 image filters.
 .
 This package contains the documentation to program with the SDL2_gfx library.
